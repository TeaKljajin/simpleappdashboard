<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!----Bootstrap--------->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container">

        <div style="text-align: center">
            <h1 style="padding-bottom: 10%;" class="mt-5">Welcome To Simple Dashboard Application</h1>
         <img src="{{ asset('images/front/el.png') }}" style="width:500px;height:300px;"><br>
            <div style="margin-top: 5%;">
            <a href="{{ route('login') }}" class="btn btn-outline-primary" style="width:120px;margin-right: 10px;">Log in</a>
            <a href="{{ route('register') }}" class="btn btn-outline-primary" style="width:120px;">Register</a>
            </div>
        </div>
     </div>

</body>
</html>

