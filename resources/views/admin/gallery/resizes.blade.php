<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <h2>Resizes Images</h2>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">Upload Images to get resize 300x300</div>
                            <div class="card-body">
                                <form method="POST" action="{{route('resizes.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="images" class="form-label">Image</label>
                                        <input type="file" class="form-control" id="images" name="imageResizes[]" multiple="" aria-describedby="images">
                                        @error('imageResizes')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            @if(session('message'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>{{session('message')}}</strong>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <div class="card-header">All Images 300x300</div>
                            <div class="card-body">

                                @if(count($resizeImages) >= 1)
                                    <div class="card-group">
                                    @foreach($resizeImages as $image)
                                        <div class="col-md-4">
                                            <img src="{{ asset($image->image) }}" style="height: 200px;width:200px;">
                                            <div style="padding-bottom:10px;"><form action="{{route('resizes.delete',$image->id)}}" method="POST">
                                                    @csrf
                                                    <input type="submit" style="padding:5px;width:200px;" value="Delete Image">
                                                </form>
                                            </div>
                                        </div>
                                    @endforeach
                                    </div>
                                    <div style="background: lightgrey;padding:5px;color:white;border-radius: 5px;">{{$resizeImages->render()}}</div>
                                @else
                                    <h6>Uploaded Images will apear hear. Currently there is no resized images. </h6>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>


