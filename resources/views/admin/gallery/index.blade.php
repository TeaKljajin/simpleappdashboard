<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <h2>Gallery</h2>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">Add Multiple Images</div>
                            <div class="card-body">
                                <div style="background: whitesmoke;padding:10px;" onclick="alert('To store multiple images just hold Shift + click');">Click here for explanation  <button type="submit" style="float:right;margin-top: -5px;" class="btn btn-small btn-secondary">Info</button></div>
                                <form method="POST" action="{{route('gallery.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="images" class="form-label">Image</label>
                                        <input type="file" class="form-control" id="images" name="images[]" aria-describedby="images" multiple="">
                                        @error('images')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            @if(session('message'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>{{session('message')}}</strong>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <div class="card-header">All Images</div>
                            <div class="card-body">
                               @if(count($images) >= 1)
                                    <div class="card-group">
                                        @foreach($images as $image)
                                            <div class="col-md-4">
                                                <img src="{{ asset($image->image) }}" style="height: 200px;width:200px;">
                                                <div style="padding-bottom:10px;"><form action="{{route('gallery.delete',$image->id)}}" method="POST">
                                                        @csrf
                                                        <input type="submit" style="padding:5px;width:200px;" value="Delete Image">
                                                    </form></div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div style="background: lightgrey;padding:5px;color:white;border-radius: 5px;">{{$images->render()}}</div>
                                @else
                                   <h6>Uploaded Images will apear hear. Currently there is no images in a gallery</h6>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

