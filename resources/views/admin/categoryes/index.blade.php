<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <h2>All Category</h2>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">Add Category</div>
                            <div class="card-body">
                                <form method="POST" action="{{route('category.store')}}">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="category_name" class="form-label">Name</label>
                                        <input type="text" class="form-control" id="category_name" name="category_name" aria-describedby="category_name"
                                               value="Add New Category" style="opacity: 0.7" onfocus="this.value=''">
                                        @error('category_name')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                        @if(session('errorMessage'))
                                            <div style="color:red;">{{ session('errorMessage') }}</div>
                                        @endif
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            @if(session('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{session('message')}}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                            @endif
                            <div class="card-header">All Category</div>
                                <div class="card-body">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">Serial No</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">User Name</th>
                                            <th scope="col">User ID</th>
                                            <th scope="col">Created_at</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($categories as $category)
                                            <tr>
                                                <th scope="row">{{$categories->firstItem()+$loop->index}}</th>
                                                <td>{{ $category->category_name}}</td>
                                                <td>{{ $category->user->name}}</td>
                                                <td>{{ $category->user->id}}</td>
                                                <td>{{ $category->created_at->diffForHumans()}}</td>
                                                <td>
                                                    <a href="{{route('category.edit',$category->id)}}" class="btn btn-small btn-info">Edit</a>
                                                    <a href="{{route('category.delete',$category->id)}}" onclick="return confirm('Are you sure to delete?')" class="btn btn-small btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{$categories->render()}}
                                </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8" style="margin-top: 15px;">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Categories Trash List</div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Serial No</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">User Name</th>
                                        <th scope="col">User ID</th>
                                        <th scope="col">Created_at</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($trashCategories as $category)
                                        <tr>
                                            <th scope="row">{{$trashCategories->firstItem()+$loop->index}}</th>
                                            <td>{{ $category->category_name}}</td>
                                            <td>{{ $category->user->name}}</td>
                                            <td>{{ $category->user->id}}</td>
                                            <td>{{ $category->created_at->diffForHumans()}}</td>
                                            <td>
                                                <a href="{{route('category.permanentlyDeleted',$category->id)}}" onclick="return confirm('After this action you will not be in position to restore your data!')" class="btn btn-small btn-danger" >Permanent Delete</a>
                                                <a href="{{route('category.restore',$category->id)}}" class="btn btn-small btn-success">Restore</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{$trashCategories->render()}}
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</x-app-layout>

