<x-app-layout>
    <div class="container" style="background: transparent url('images/iceberg-background.jpg') no-repeat center center;">
    <h1 style="text-align: center;margin-top: 3%;padding-top: 10%;">Basic Statistics</h1>
        <div class="row" style="margin-top: 10%;padding-bottom: 20%;">
            <div class="col-md-3">
                <div class="card" style="text-align: center;">
                    <div class="card-header">
                        All Users
                    </div>
                    <div class="card-body">
                        {{count($users)}}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card"style="text-align: center;">
                    <div class="card-header">
                        All Categories
                    </div>
                    <div class="card-body">
                        {{count($categories)}}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" style="text-align: center;">
                    <div class="card-header">
                        All Brands
                    </div>
                    <div class="card-body">
                        {{count($brands)}}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" style="text-align: center;">
                    <div class="card-header">
                        All Resizes Image
                    </div>
                    <div class="card-body">
                        {{count($resizes)}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
