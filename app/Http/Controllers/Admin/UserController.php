<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }


    public function index(){
        $users = User::latest()->paginate(15);
        return view('admin.users.index',compact('users'));
    }


    public function store(Request $request){
        $validated = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|unique:user|max:255',
            'password' => 'required|min:8|max:255',

        ],[
            'name.required' => 'These field is required',
            'name.max' => 'You can put max 255 characters',
            'email.required' => 'These field is required',
            'email.max' => 'You can put max 255 characters',
            'password.required' => 'These field is required',
            'password.min' => 'You can put min 8 characters',
            'password.max' => 'You can put max 255 characters',
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        session()->flash('message','You created successfully user');
        return redirect(route('user.index'));
    }





}
