<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Resize;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function __construct(){
        return $this->middleware('auth');
    }

    public function index(){
        $users = User::all();
        $categories = Category::all();
        $brands = Brand::all();
        $resizes = Resize::all();
        return view('dashboard',compact('users','categories','brands','resizes'));
    }


}
