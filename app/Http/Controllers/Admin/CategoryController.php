<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{


    public function __construct(){
        return $this->middleware('auth');
    }

    public function index(){
        $categories = Category::latest()->paginate(5);
        $trashCategories = Category::onlyTrashed()->latest()->paginate(5);
        return view('admin.categoryes.index',compact('categories','trashCategories'));
    }



    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'category_name' => 'required|unique:categories,category_name|min:2|max:255',

        ]);

        if ($validator->fails()) {
            return redirect()->route('category.index')
                ->withErrors($validator)
                ->withInput();
        }elseif($request->category_name === 'Add New Category'){
            return redirect('category/index')->with('errorMessage','You must type new name');
        }else{
            Category::insert([
                'category_name' => $request->category_name,
                'user_id' => Auth::user()->id,
                'created_at' => Carbon::now(),
            ]);
            session()->flash('message','You created successfully category');
            return redirect()->route('category.index');
        }

    }



    public function edit($id){
          $categories = Category::latest()->paginate(15);
          $category = Category::find($id);
          return view('admin.categoryes.edit',compact('category','categories'));

    }


    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|unique:categories,category_name|min:2|max:255',

        ]);

        if ($validator->fails()) {
            return redirect()->route('category.edit',$request->id)
                ->withErrors($validator)
                ->withInput();
        }else{
            $category = Category::find($id);
            $category->update([
                'category_name' => $request->category_name,
                'user_id' => Auth::user()->id,
                'created_at' => Carbon::now(),
            ]);
            session()->flash('message','You updated successfully category');
            return redirect()->route('category.index');

        }

    }


    public function delete($id){
        $category = Category::find($id);
        $category->delete();
        session()->flash('message','You deleted successfully category');
        return redirect()->route('category.index');
    }

    public function restore($id){
        $category = Category::withTrashed()->find($id)->restore();
        session()->flash('message','You restored successfully category');
        return redirect()->route('category.index');
    }

    public function permanentlyDeleted($id){
        $category = Category::onlyTrashed()->find($id)->forceDelete();
        session()->flash('message','You deleted permanently category');
        return redirect()->route('category.index');

    }


}
