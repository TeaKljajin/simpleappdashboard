<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Resize;
use Intervention\Image\Facades\Image;



class GalleryController extends Controller
{

    public function __construct(){
        return $this->middleware('auth');
    }


    public function index(){
        $images = Gallery::latest()->paginate(9);
        return view('admin.gallery.index',compact('images'));
    }


    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'images' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('gallery.index')
                ->withErrors($validator)
                ->withInput();
        } else{
            //Prepare Images
            $images = $request->file('images');

            foreach($images as $image){
                $images_ext = strtolower($image->getClientOriginalExtension());
                $name_gen = hexdec(uniqid());
                $up_location = "images/gallery/";
                $img_name = $name_gen. '.' .$images_ext;
                $image->move($up_location,$img_name);
                $name_for_db = $up_location.$img_name;
                Gallery::insert([
                    'image' => $name_for_db,
                    'created_at' => Carbon::now(),
                ]);
            }

            session()->flash('message','You created successfully gallery');
            return redirect()->route('gallery.index');
        }
    }

    public function delete($id){
        $imageGallery = Gallery::find($id);
        $image = $imageGallery->image;
        unlink($image);

        Gallery::find($id)->delete();
        session()->flash('message','You deleted image successfully');
        return redirect()->route('gallery.index');
    }




    //Start Everything for Resize Model
    public function resizes(){
        $resizeImages = Resize::latest()->paginate(9);
        return view('admin.gallery.resizes',compact('resizeImages'));
    }



    public function resizesStore(Request $request){

        $validator = Validator::make($request->all(), [
            'imageResizes' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->route('resizes')
                ->withErrors($validator)
                ->withInput();
        } else{
            //Prepare Image
            $images = $request->file('imageResizes');
            foreach($images as $image){
                $image_ext = strtolower($image->getClientOriginalExtension());
                $name_gen = hexdec(uniqid());
                $up_location = "images/resizes300/";
                $img_name = $name_gen. '.' .$image_ext;
//            $image->move($up_location,$img_name);
                Image::make($image)->resize(300,300)->save($up_location.$img_name);

                $name_for_db = $up_location.$img_name;

                Resize::insert([
                    'image' => $name_for_db,
                    'created_at' => Carbon::now(),
                ]);

            }

            session()->flash('message','You created successfully image300x300');
            return redirect()->route('resizes');
        }

    }


    public function resizesDelete($id){
        $imageResize = Resize::find($id);
        $image = $imageResize->image;
        unlink($image);

        Resize::find($id)->delete();
        session()->flash('message','You deleted image successfully');
        return redirect()->route('resizes');
    }



}
