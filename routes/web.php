<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\GalleryController;


use App\Http\Controllers\Front\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Email Verification For New Registered Users
Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');


//Front
Route::get('/', [LoginController::class,'index'])->name('index');


//Dashboard
Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard');

//User
Route::get('/users', [UserController::class,'index'])->name('user.index');
Route::post('/store', [UserController::class,'store'])->name('user.store');

//Category
Route::get('/category/index', [CategoryController::class,'index'])->name('category.index');
Route::post('/category/store', [CategoryController::class,'store'])->name('category.store');
Route::get('/category/edit/{id}', [CategoryController::class,'edit'])->name('category.edit');
Route::post('/category/update/{id}', [CategoryController::class,'update'])->name('category.update');
Route::get('/category/delete/{id}', [CategoryController::class,'delete'])->name('category.delete');
Route::get('/category/restore/{id}', [CategoryController::class,'restore'])->name('category.restore');
Route::get('/category/permanentlyDeleted/{id}', [CategoryController::class,'permanentlyDeleted'])->name('category.permanentlyDeleted');


//Brand
Route::get('/brand/index', [BrandController::class,'index'])->name('brand.index');
Route::get('/brand/edit/{id}', [BrandController::class,'edit'])->name('brand.edit');
Route::post('/brand/store', [BrandController::class,'store'])->name('brand.store');
Route::post('/brand/update/{id}', [BrandController::class,'update'])->name('brand.update');
Route::get('/brand/delete/{id}', [BrandController::class,'delete'])->name('brand.delete');
Route::get('/brand/restore/{id}', [BrandController::class,'restore'])->name('brand.restore');
Route::get('/brand/permanentlyDeleted/{id}', [BrandController::class,'permanentlyDeleted'])->name('brand.permanentlyDeleted');

//Gallery
Route::get('/gallery/index', [GalleryController::class,'index'])->name('gallery.index');
Route::post('/gallery/store', [GalleryController::class,'store'])->name('gallery.store');
Route::post('/gallery/delete/{id}', [GalleryController::class,'delete'])->name('gallery.delete');

//Resize
Route::get('/resizes', [GalleryController::class,'resizes'])->name('resizes');
Route::post('/resizes/store', [GalleryController::class,'resizesStore'])->name('resizes.store');
Route::post('/resizes/delete/{id}', [GalleryController::class,'resizesDelete'])->name('resizes.delete');











